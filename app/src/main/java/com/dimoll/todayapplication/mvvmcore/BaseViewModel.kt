package com.dimoll.todayapplication.mvvmcore

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData

open class BaseViewModel(application: Application) : AndroidViewModel(application) {
    val ldErrorToast = MutableLiveData<String?>()

    protected fun showToast(error: String?) {
        ldErrorToast.postValue(error)
    }
}