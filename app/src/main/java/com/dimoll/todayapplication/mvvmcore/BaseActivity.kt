package com.dimoll.todayapplication.mvvmcore

import android.app.Application
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

abstract class BaseActivity<VM : BaseViewModel, B : ViewDataBinding> : AppCompatActivity() {
    protected abstract val layoutId: Int
    protected val viewModel: VM by lazy { createViewMode() }
    protected lateinit var binding: B

    abstract fun createViewMode(): VM
    abstract fun setDataToBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, layoutId)
        setDataToBinding()
        setObservers()
    }

    private fun setObservers() {
        viewModel.ldErrorToast.observe(this, Observer {
            it ?: return@Observer
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
    }

    protected inline fun <reified VMP : VM> provideViewModel(noinline instance: (application: Application) -> VMP): VMP {
        return ViewModelProviders.of(this, ViewModelFactory(application, instance)).get(VMP::class.java)
    }
}