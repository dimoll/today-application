package com.dimoll.todayapplication.mvvmcore

import android.app.Application
import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.arch.lifecycle.ViewModelProviders

abstract class BaseFragment<VM : BaseViewModel, B : ViewDataBinding> : Fragment() {
    protected abstract val layoutId: Int
    protected val viewModel: VM by lazy { createViewMode() }
    protected lateinit var binding: B

    abstract fun createViewMode(): VM
    abstract fun setDataToBinding()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(layoutInflater, layoutId, container, false)
        setDataToBinding()
        setObservers()

        return binding.root
    }

    private fun setObservers() {
        viewModel.ldErrorToast.observe(this, Observer {
            it ?: return@Observer
            Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
        })
    }

    protected inline fun <reified VMP : VM> provideViewModel(noinline instance: (application: Application) -> VMP): VMP {
        return ViewModelProviders.of(this, ViewModelFactory(activity?.application!!, instance)).get(VMP::class.java)
    }
}