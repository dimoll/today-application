package com.dimoll.todayapplication.ui.news.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import com.dimoll.todayapplication.databinding.ItemNewsBinding
import com.dimoll.todayapplication.ui.news.entities.News
import com.squareup.picasso.Picasso


class NewsViewHolder(private val binding: ItemNewsBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(inputNews: News) {
        Picasso
            .get()
            .load(inputNews.urlToImage)
            .into(binding.imageNews)
        with(binding) {
            news = inputNews
            imageNews.visibility = if (inputNews.urlToImage != null) View.VISIBLE else View.GONE
            authorNews.visibility = if (inputNews.author != null) View.VISIBLE else View.GONE
            executePendingBindings()
        }
    }
}