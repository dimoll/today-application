package com.dimoll.todayapplication.ui.weather.enteties

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Coord {
    @SerializedName("lon")
    @Expose
    var lon: Float = 0.toFloat()
    @SerializedName("lat")
    @Expose
    var lat: Float = 0.toFloat()
}