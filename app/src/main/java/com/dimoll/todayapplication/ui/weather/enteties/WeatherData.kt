package com.dimoll.todayapplication.ui.weather.enteties

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WeatherData {

    @SerializedName("coord")
    @Expose
    var coord: Coord? = null

    @SerializedName("weather")
    @Expose
    var weather: List<Weather>? = null

    @SerializedName("base")
    @Expose
    var base: String? = null

    @SerializedName("main")
    @Expose
    var main: Main? = null

    @SerializedName("visibility")
    @Expose
    var visibility: Int = 0

    @SerializedName("wind")
    @Expose
    var wind: Wind? = null

    @SerializedName("clouds")
    @Expose
    var clouds: Clouds? = null

    @SerializedName("rain")
    @Expose
    var rain: Rain? = null

    @SerializedName("snow")
    @Expose
    var snow: Snow? = null

    @SerializedName("dt")
    @Expose
    var dt: Long = 0

    @SerializedName("sys")
    @Expose
    var sys: Sys? = null

    @SerializedName("timezone")
    @Expose
    var timezone: Int = 0

    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("cod")
    @Expose
    var cod: Int = 0
}