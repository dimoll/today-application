package com.dimoll.todayapplication.ui.weather.enteties

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Rain {
    @SerializedName("3h")
    @Expose
    var volume3h: Int = 0


    @SerializedName("1h")
    @Expose
    var volume1h: Int = 0
}