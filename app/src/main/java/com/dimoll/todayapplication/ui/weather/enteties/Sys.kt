package com.dimoll.todayapplication.ui.weather.enteties

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Sys {
    @SerializedName("type")
    @Expose
    var type: Int = 0
    @SerializedName("id")
    @Expose
    var id: Int = 0
    @SerializedName("message")
    @Expose
    var message: Float = 0.toFloat()
    @SerializedName("country")
    @Expose
    var country: String? = null
    @SerializedName("sunrise")
    @Expose
    var sunrise: Int = 0
    @SerializedName("sunset")
    @Expose
    var sunset: Int = 0
}
