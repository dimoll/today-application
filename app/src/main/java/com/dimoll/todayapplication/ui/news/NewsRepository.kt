package com.dimoll.todayapplication.ui.news

import android.arch.lifecycle.MutableLiveData
import com.dimoll.todayapplication.apihelpers.NewsApi
import com.dimoll.todayapplication.apihelpers.NewsService
import com.dimoll.todayapplication.ui.news.entities.NewsData
import retrofit2.Call
import retrofit2.Response

class NewsRepository {
    private val apiHelper: NewsApi = NewsService.instance.getJSONApi()

    fun onGetNews(
        country: String = "ua",
        pageSize: Int = 50,
        apiKey: String = NewsService.api_key
    ): MutableLiveData<NewsData> {

        val newsMessage = MutableLiveData<NewsData>()

        apiHelper.getNews(country, pageSize, apiKey)
            .enqueue(object : retrofit2.Callback<NewsData> {
                override fun onResponse(call: Call<NewsData>, response: Response<NewsData>) {
                    if (response.isSuccessful) {
                        newsMessage.value = response.body()
                    }
                }

                override fun onFailure(call: Call<NewsData>, t: Throwable) {
                    newsMessage.value = null
                }
            })

        return newsMessage
    }
}
