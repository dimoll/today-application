package com.dimoll.todayapplication.ui.news

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.dimoll.todayapplication.R
import com.dimoll.todayapplication.databinding.ActivityNewsBinding
import com.dimoll.todayapplication.mvvmcore.BaseActivity
import com.dimoll.todayapplication.ui.news.adapters.NewsAdapter
import com.dimoll.todayapplication.ui.news.entities.News
import java.util.*

class NewsActivity : BaseActivity<NewsViewModel, ActivityNewsBinding>() {
    private val tag = this::class.java.simpleName

    override val layoutId: Int = R.layout.activity_news
    private var newsAdapter: NewsAdapter? = null
    var newsArrayList = ArrayList<News>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.onGetNews()

        setObservers()

        initRecyclerView()

    }

    private fun initRecyclerView() {
        if (newsAdapter == null) {
            newsAdapter = NewsAdapter()
            newsAdapter?.setData(newsArrayList)
            with(binding) {
                newsRecyclerView.layoutManager = LinearLayoutManager(this@NewsActivity)
                newsRecyclerView.adapter = newsAdapter
                newsRecyclerView.itemAnimator = DefaultItemAnimator()
                newsRecyclerView.isNestedScrollingEnabled = true
            }
        } else {
            newsAdapter!!.notifyDataSetChanged()
        }
    }

    private fun setObservers() {
        viewModel.mutableNewsData?.observe(this, Observer {
            val newsList: List<News>? = it?.newsArray
            if (newsList != null) {
                newsArrayList.addAll(newsList)
            }
            newsAdapter?.setData(newsArrayList)
            newsAdapter?.notifyDataSetChanged()
        })
    }

    override fun createViewMode(): NewsViewModel = provideViewModel { NewsViewModel(it) }

    override fun setDataToBinding() {
    }
}
