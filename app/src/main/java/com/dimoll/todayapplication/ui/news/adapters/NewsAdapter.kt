package com.dimoll.todayapplication.ui.news.adapters

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dimoll.todayapplication.R
import com.dimoll.todayapplication.databinding.ItemNewsBinding
import com.dimoll.todayapplication.ui.news.entities.News
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.net.Uri


class NewsAdapter : RecyclerView.Adapter<NewsViewHolder>() {

    private var newsList = ArrayList<News>()

    fun setData(news: Collection<News>) {
        newsList.clear()
        newsList.addAll(news)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemNewsBinding = DataBindingUtil.inflate(inflater, R.layout.item_news, parent, false)
        return NewsViewHolder(binding)
    }

    override fun getItemCount(): Int = newsList.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(newsList[position])
    }

}