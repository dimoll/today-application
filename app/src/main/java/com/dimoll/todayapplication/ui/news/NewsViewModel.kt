package com.dimoll.todayapplication.ui.news

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.dimoll.todayapplication.mvvmcore.BaseViewModel
import com.dimoll.todayapplication.ui.news.entities.NewsData

class NewsViewModel(application: Application) : BaseViewModel(application) {
    private val repository = NewsRepository()

    var mutableNewsData: MutableLiveData<NewsData>? = null

    fun onGetNews() {
        if (mutableNewsData == null) {
            mutableNewsData = repository.onGetNews()
        }
    }
}