package com.dimoll.todayapplication.apihelpers

import com.dimoll.todayapplication.ui.news.entities.NewsData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    @GET("/v2/top-headlines")
    fun getNews(
        @Query("country") country: String,
        @Query("pageSize") pageSize: Int,
        @Query("apiKey") spiKey: String
    ): Call<NewsData>
}