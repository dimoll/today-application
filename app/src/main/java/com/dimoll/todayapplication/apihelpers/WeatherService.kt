package com.dimoll.todayapplication.apihelpers

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WeatherService private constructor() {

    private val mRetrofit: Retrofit

    fun JsonWeatherApi(): WeatherApi = mRetrofit.create<WeatherApi>(
        WeatherApi::class.java
    )


    init {
        mRetrofit = Retrofit.Builder()
            .baseUrl(base_url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    companion object {
        private var mInstance: WeatherService? = null

        const val api_key = "888d24e471004d1b52192614302c5af3"
        private const val base_url =
            "https://openweathermap.org/api/"

        val instance: WeatherService
            get() {
                if (mInstance == null) {
                    mInstance =
                        WeatherService()
                }
                return mInstance!!
            }
    }
}
