package com.dimoll.todayapplication.apihelpers

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NewsService private constructor() {

    private val mRetrofit: Retrofit

    fun getJSONApi(): NewsApi = mRetrofit.create<NewsApi>(
        NewsApi::class.java
    )

    init {
        mRetrofit = Retrofit.Builder()
            .baseUrl(base_url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    companion object {
        private var mInstance: NewsService? = null
        const val api_key = "9b615245f0194917b9e3433b6346cf8c"
        private const val base_url =
            "https://newsapi.org/"

        val instance: NewsService
            get() {
                if (mInstance == null) {
                    mInstance =
                        NewsService()
                }
                return mInstance!!
            }
    }
}