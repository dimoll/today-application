package com.dimoll.todayapplication.apihelpers

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("/data/2.5/weather")
    fun getWeather(@Query("q") name: String, @Query("pageSize") size: Int, @Query("APPID") surname: String): Call<WeatherApi>

}